import time,os,sys
from pprint import pprint
import numpy as np

sys.path.append(os.path.abspath("./utils/"))
from Circ_Util import  write_yaml, read_yaml, npAr_avr_and_err, read_data_hdf5, write_data_hdf5

#...!...!....................
def import_yieldArray( jid6, dataPath,metaD0):
        inpF=dataPath+'%s-meta.yaml'%jid6
        metaD1=read_yaml(inpF)
        print('metaD keys:', metaD1.keys())

        # verify consistency
        for x in [ 'meas_lab','exp_name' ]:
            assert  metaD0['retr_info'][x]== metaD1['retr_info'][x]

        for x in ['transpName', 'shots']:
            assert  metaD0['subm_info'][x]== metaD1['subm_info'][x]
                
        inpH5=dataPath+metaD1['retr_info']['counts_fname']
        blob= read_data_hdf5(inpH5)
        return metaD1,blob['counts']


#............................
#............................
#............................
class OneExp_Ana(object):
    def __init__(self, expId,labels,metaD):
        self.expId=expId
        self.labels=labels
        self.jid6L=[]  # keep track of all jobs
        self.runTimeL=[]
        self.runDateL=[]
        self.rawY=np.array([0]) # yield[label][run], run=cyc1+cyc2+... 
        self.shots=metaD['subm_info']['shots']
        self.metaD=metaD
        
#...!...!....................
    def unpack_yieldArray(self,jid6, runDate, runTime, countsAr):
        self.jid6L.append(jid6)
        self.runDateL.append(runDate)
        self.runTimeL.append(runTime)
        if self.rawY.ndim==1:
            self.rawY=countsAr
        else:
            #print('qq',self.rawY.shape,countsAr.shape,self.expId)
            rawY=np.concatenate((self.rawY,countsAr), axis=1)
            #print('qq2',rawY.shape)
            self.rawY=rawY        
            
#...!...!....................
    def analyzeIt(self):
        #print('ana expId=',self.expId)
        assert self.rawY.ndim==2
       
        probL=[]; probA=[]
        for iLab,lab in enumerate(self.labels):
            Y=self.rawY[iLab,:].flatten()
            # inflate to 1  yields <1. - to compensate the error mitigation 
            Y[Y<1.0]=1. # is this correct?
            num=Y.shape[0]
            Prob_lab=Y/self.shots  # prob(lab)
            
            probL.append(Prob_lab)  # probability  per experiment
            avrProb=npAr_avr_and_err(Prob_lab)

            probA.append(avrProb)  # probability averaged over exp.
              
        self.dataV={'prob':probL, 'avr_prob':probA}
        # convert all to np-array, prob:[label][run], avr_prob:[label]
        for k in self.dataV:
            ar=np.array(self.dataV[k])
            #print('ana-end key=',k,ar.shape)
            self.dataV[k]=ar
        
