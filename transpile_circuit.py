#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''

Part 1) use Qiskit to transpile (optimize, L=1 for back=simu)

required INPUT:
--circName: ghz_5qm

'''

# Import general libraries (needed for functions)
import time,os,sys
from pprint import pprint
import numpy as np
from qiskit import QuantumCircuit, transpile 

sys.path.append(os.path.abspath("./utils/"))
from Circ_Util import  write_yaml,circuit_summary, circ_2_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('-i',"--inpPath",default='circ_ideal',help="input for everything")

    parser.add_argument("-c","--circName", default='bv_4Q',
                        help="name of ideal  circuit")
    parser.add_argument('-O','--optimLevel',type=int,default=1, help="transpiler optimization 0:just converts gates, 1:agregates , 2: changes mapping")
    parser.add_argument('-r',"--rnd_seed",default=123,type=int,help="for transpiler")

    parser.add_argument('-o',"--outPath",default='out',help="output path ")
    
    args = parser.parse_args()
    args.inpPath+='/' 
    args.outPath+='/' 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [args.inpPath, args.outPath]:
        if  os.path.exists(xx): continue
        print('missing dir: ',xx)
        exit(1)

    return args


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()

metaD={}

circF=args.inpPath+'/'+args.circName+'.qasm'
circOrg=QuantumCircuit.from_qasm_file( circF )
circOrg.name=args.circName
print('\ncirc original: %s, depth=%d'%( circF,circOrg.depth()))
print(circOrg)
metaD['circOrg']=circ_2_yaml(circOrg)
#


basis_gates = ['u1','u2','u3','cx'] 
print('Assume basis gates:',basis_gates)

print(' Layout using optimization_level=',args.optimLevel)
circOpt = transpile(circOrg, basis_gates=basis_gates, optimization_level=args.optimLevel, seed_transpiler=args.rnd_seed) 
print(circOpt)
circOpt.name=args.circName+'+o%d'%args.optimLevel

metaD['circOpt']=circ_2_yaml(circOpt, verb=0)
metaD['circOpt']['backend']='qasm_simulator'
metaD['circOpt']['seed']=args.rnd_seed
metaD['circOpt']['optimLevel']=args.optimLevel

transpF=args.outPath+'/transp.'+circOpt.name+'.yaml'
write_yaml(metaD,transpF)


pprint(metaD['circOpt']['info'])




