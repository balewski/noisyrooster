#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo 'packing-driver start on '`hostname`' '`date`
env|grep  SHIFTER_RUNTIME

nameList=$1
numCycle=${2-50}
procIdx=${SLURM_PROCID}
maxProc=`cat $nameList |wc -l`

jid6=${SLURM_JOB_ID:(-6)}_$procIdx
echo my procIdx=$procIdx   maxProc=$maxProc pwd:`pwd`
echo RANK_`hostname` $SLURM_PROCID numCycle=$numCycle

export OPENBLAS_NUM_THREADS=2
# to avoid: OpenBLAS blas_thread_init: pthread_create failed

if [ $procIdx -ge $maxProc ]; then
   echo "rank $procIdx above maxProc=$maxProc, idle ..."; exit 0
fi

line=`head -n $[ $procIdx +1 ] $nameList |tail -n 1`
echo Do=${line}=

#run string with values as a command 
eval "$line --jid6 $jid6 " >& log.sim.$procIdx

echo 'simu $procIdx done  on  '`date` 
./ana_sim.py -d out --targetLabel $targetLabel --noXterm -j j$jid6  >& log.ana.$procIdx
echo 'ana $procIdx done  on  '`date` 
