import numpy as np
from pprint import pprint
import time, os
import h5py
import hashlib
import ruamel.yaml  as yaml
from qiskit.converters import circuit_to_dag


#...!...!....................
def unroll_jid6(kL):
    # expand list if '-' are present
    kkL=[]
    for x in kL:
        if '-' not in x:
            kkL.append(x) ; continue
        x1=x.split('_')
        x2=x1[1].split('-')
        for i in range(int(x2[0]),int(x2[1])+1):
            kkL.append('%s_%d'%(x1[0],i))
    print('U:unroll_jid6:',kL,'  to ',kkL)
    kL=kkL
    
    nK=len(kL)
    assert nK>0
    return kL
  

#...!...!....................
def yields_to_numpy(yieldL,outD):
    nclbit=len(outD['job_info']['meas_qubit'])
    exp_nameL= outD['job_info']['exp_name']
    shots=outD['subm_info']['shots']
    #pprint(yieldL)
    #print('eee',exp_nameL)
    labL=[] # regenarate  them
    for i in range(2**nclbit):
        #bitStr=bin(i)[2:].zfill(nclbit)
        bitStr='0x%0x'%i
        labL.append(bitStr)
    print(1<<nclbit,'state_labels:',' '.join(labL))
    num_lab=len(labL)
    num_exp=len(exp_nameL)
    num_cyc=outD['retr_info']['num_cycles']
    yar_shape=(num_exp,num_lab,num_cyc)
    print('yar_shape=',yar_shape)
    outD['retr_info']['meas_lab']=labL
    outD['retr_info']['exp_name']=exp_nameL

    yar=np.zeros(yar_shape)
    for ic in range(num_cyc):
        for ie in range(num_exp):
            rec=yieldL[ic*num_exp+ie]
            name=rec['name']
            counts=rec['counts']
            if ic==0 and 'base' in name:
                print('YAR:',ic,ie,name,counts)
            assert shots==rec['shots']
            assert name==exp_nameL[ie]
            for lab in counts:
                ilab=labL.index(lab)
                #print('ii',ilab,lab)
                yar[ie,ilab,ic]=counts[lab]

    print('yar',yar.shape)
    return {'counts':yar.astype(np.float32),'shots':np.array([shots]).astype(np.int32)}
    
#...!...!....................
def npAr_avr_and_err(yV, axis=None):
    if axis==None:
        num=yV.size
    else:
        num=yV.shape[axis]
    avr=np.mean(yV,axis=axis)
    std=np.std(yV,axis=axis)
    err=std/np.sqrt(num) # it is error of the avr
    return (avr,std,num,err)



#...!...!....................
def circuit_summary(circ, verb=1):
    dag = circuit_to_dag(circ)
    dagProp=dag.properties()
    #pprint(dagProp)
    ''' Explanation
    -factors = layers=  how many components the circuit can decompose into
    -depth = factors+barriers
    -size = gates+barriers+measuerements
    -qubits = total in the device
    '''
    cnt={'cx':{},'u1':{},'u2':{},'u3':{}}
    outD={}
    for x in ['operations', 'depth','size','factors','qubits']:
        outD[x]=dagProp[x]
    #.... add non-present gates count as 0s
    ciop=outD['operations']
    for x in cnt:
        if x not in ciop: ciop[x]=0
                
    #.... capture used & measure qubits
    gateL=dag.op_nodes()  # will include bariers
    gates=0
    mquL=[]; uquS=set(); mclL=[]

    for idx,node in enumerate(gateL):
        if node.name=='barrier': continue
        if node.name=='measure':
            q=node.qargs[0].index
            mquL.append(q); uquS.add(q)
            c=node.cargs[0].index
            mclL.append(c)
            continue
        # below proecess 1 & 2 qubit gates
        gates+=1
        # a) collect all used qubits, to find ancella
        for qu in node.qargs:
             uquS.add(qu.index)

        # b) counnt cx gates for every qubit pair in every direction
        if node.name=='cx':
            qL=[ qu.index for qu in node.qargs]
            qT=tuple(qL)
            if qT not in cnt['cx']: cnt['cx'][qT]=0
            cnt['cx'][qT]+=1
        if node.name in ['u1','u2', 'u3']:
            nm=node.name
            q=node.qargs[0].index
            if q not in cnt[nm]: cnt[nm][q]=0
            cnt[nm][q]+=1
             
    assert gates>=1
    #print('gate cnt:',cnt)
    outD['gateCnt']=cnt
    #print('used qu=',sorted(list(uquS)))
    outD['meas_clbit']=mclL
    outD['meas_qubit']=mquL
    outD['ancilla']=sorted(list(uquS-set(mquL)))
    outD['gates']=gates
    outD['name']=circ.name
    if verb>0:
        print('\nsummary=',outD)
    #print(outD.keys())
    return outD
    


#...!...!....................
def circ_2_yaml(circ,verb=1):  # packs circuit data for saving
    outD={}
    outD['info']=circuit_summary(circ,verb)
    outD['qasm']=circ.qasm()

    hao = hashlib.md5(outD['qasm'].encode())
    outD['md5']=hao.hexdigest()
    #pprint(outD)
    return outD



#...!...!....................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!....................
def write_yaml(rec,ymlFn,verb=1):
        startT = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1024
        if verb:
            print('  closed:   ',ymlFn,' size=%.2f kB'%xx,'  elaT=%.1f sec'%(time.time() - startT))

#...!...!..................
def write_data_hdf5(dataD,outF,verb=1):
    h5f = h5py.File(outF, 'w')
    if verb>0: print('saving data as hdf5:',outF)
    for item in dataD:
        rec=dataD[item]
        h5f.create_dataset(item, data=rec)
        if verb>0:print('h5-write :',item, rec.shape,rec.dtype)
    h5f.close()
    xx=os.path.getsize(outF)/1048576
    print('closed  hdf5:',outF,' size=%.2f MB'%(xx))

    
#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape,obj.dtype)
            objD[x]=obj

        h5f.close()
        return objD
