#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
Part 6) Analyze yields from many Grover jobs

Required INPUT:
   --jid6 list: j33ab44

'''
import matplotlib.pyplot as plt
# Import general libraries (needed for functions)
import time,os,sys
from pprint import pprint
import numpy as np
from dateutil.parser import parse as date_parse

from OneExp_Ana import OneExp_Ana, import_yieldArray
from OneExp_AnaPlotter import OneExp_AnaPlotter

sys.path.append(os.path.abspath("./utils/"))
from Circ_Util import  write_yaml, read_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('-d',"--dataPath",default='jobs',help="input for everything")
    parser.add_argument('-o',"--outPath",default='out',help="output path for plots")
    parser.add_argument( "-X","--noXterm",dest='noXterm',action='store_true',
                         default=False, help="disable X-term for batch mode")
    parser.add_argument('-j',"--jid6", nargs='+',default=['jf02d1c'] ,help="shortened job ID list, blank separated ")

    parser.add_argument('-T',"--targetLabel",default='0x5',help="target lable for injection scan plot")


    args = parser.parse_args()
    args.dataPath+='/' 
    args.outPath+='/'
    args.baseExp='0.base.0'
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()

''' structure of yieldArray data:
 - multiple Qiskit jobs, each has own yieldArray
 - np-array comntains mutiple cycles of sequence of experiments
 Processing:
 - a) sort experiments into the same type (done by AnaOne)
 - b) compute fidelity of each experiment
 - c) compute std dev of fidelity over many experiments of the same type
 - d) plot results
 - e) save summary
'''

print('read %s job yields'%len(args.jid6))

# (1) read 1st experimet to setup all variables
inpF=args.dataPath+'%s-meta.yaml'%args.jid6[0]
metaD=read_yaml(inpF)
print('metaD keys:', metaD.keys())
labL=metaD['retr_info']['meas_lab']
expIdL=metaD['retr_info']['exp_name']
num_exp=len(expIdL)
args.prjName=metaD['subm_info']['transpName']

plot=OneExp_AnaPlotter(args)


assert args.targetLabel in labL  #move it up after meta-data file is added

# (3)  prime OneExp_Ana for each expId
oneD={} # key=expId
for expId in expIdL:
    oneD[expId]=OneExp_Ana(expId,labL,metaD)

# (4) sum experiments for the same expId
numRun=0; totRunTime=0
for jid6 in args.jid6:
    print('injest jid6=',jid6)
    metaD1,yAr=import_yieldArray(jid6,args.dataPath,metaD)
    numRun+=yAr.shape[2]
    runDate=date_parse(metaD1['retr_info']['exec_date'])
    runTime=metaD1['retr_info']['simu_elaT']
    totRunTime+=runTime
    for ie,expId in enumerate(expIdL):
        #print('aa',ie,expIdL)
        oneD[expId].unpack_yieldArray(jid6, runDate,runTime, yAr[ie])
print('\nM: imported %d jobs, numRun=%d, elaT=%.1f min, injPt=%d \n'%(len(args.jid6),numRun,totRunTime/60.,len(oneD)))

for expId in expIdL:
    oneD[expId].analyzeIt()
        
# - - - - - - - -
# PLOT RESULTS
# - - - - - - - -
    
#baseExp='10.cx.0.2'

# track time stability
#one=oneD[baseExp]
#plot.one_label_vs_date(one,args.targetLabel,obsN='counts',figId=10,tit1='baseExp='+baseExp)


# selected results
for expId in [args.baseExp] :
    #break
    one=oneD[expId]
    #1plot.histo_per_label(one,obsN='prob',figId=100)
    plot.experiment_summary(one, obsN='prob',figId=300)
    break

noiseType=metaD['job_info']['noiseConf']['type']

nameF=args.jid6[0]
if len(args.jid6)>1:
    nameF='%sx%d'%(args.jid6[0],len(args.jid6))

if len(expIdL)>1 and 1:
    if noiseType=='gate':
        dataV=plot.multi_experiment_gate(oneD,args.targetLabel,expIdL,figId=400)
    elif noiseType=='qubit':
        dataV=plot.multi_experiment_qubit(oneD,args.targetLabel,expIdL,figId=400)
    else:
        print('bad1 noise type=%s, ABORT'%noiseType)
        exit(11)

    outF=args.outPath+'%s-%sAna.yaml'%(nameF,noiseType)
    
    rec={'jid6':args.jid6,'data':dataV,'noiseConf':metaD['job_info']['noiseConf'],'targetLabel':args.targetLabel,'transpName':args.prjName,'circ_info':metaD['circ_info'],'subm_info':metaD['subm_info']}
    write_yaml(rec,outF)

plot.display_all('ana-'+nameF, tight=True,pdf=2)

