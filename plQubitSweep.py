#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
Plot sweep of fidelity for noisy qubits

Required INPUT:
   --jid6 list: j33ab44 j342h435

'''
# Import general libraries (needed for functions)
import time,os,sys
from pprint import pprint
import numpy as np
from dateutil.parser import parse as date_parse


sys.path.append(os.path.abspath("./utils/"))
from Circ_Util import  write_yaml, read_yaml, unroll_jid6
from Plotter_Backbone import Plotter_Backbone


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('-d',"--dataPath",default='jobs',help="input for everything")
    parser.add_argument('-o',"--outPath",default='out',help="output path for plots")
    parser.add_argument( "-X","--no-Xterm",dest='noXterm',action='store_true',
                         default=False, help="disable X-term for batch mode")
    parser.add_argument('-j',"--jid6", nargs='+',default=['jf02d1c'] ,help="shortened job ID list, blank separated, takes n1-n2 ")

    args = parser.parse_args()
    args.dataPath+='/' 
    args.outPath+='/'
    args.baseExp='0.base.0'
    args.jid6=unroll_jid6(args.jid6)
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class SweepPlotter(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#...!...!....................
    def qubit_summary(self,labL,thetaV,dataV,figId=1,tit1=''):        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,5))
        nrow,ncol=1,2
        

        nQ=len(labL)-1
        fac1=[  int( x.split('.')[2]) for x in labL[1:] ]                
        thetaV2=np.square(thetaV)
        print('fac1',fac1)
        mL=['>','s','o','D','*','X','<','^','v']

        _,axA = self.plt.subplots(nrow,ncol,num=figId)

        for iq in range(nQ):
            block=dataV[:,iq+1]
            #print('bb',iq,block)
            ax=axA[1]
            ax.errorbar(thetaV, block[:,0], yerr= block[:,1],label=labL[iq+1], marker=mL[iq])
            ax.set(xlabel='noise std dev')
            
            ax=axA[0]
            ax.errorbar(thetaV2*fac1[iq], block[:,0], yerr= block[:,1],label=labL[iq+1], marker=mL[iq])
            ax.set(xlabel='(noise std dev)^2 * (num gates)')

        ax.set(ylabel='fidelity',title=args.prjName)
        ax.legend(loc='upper right', title='qubit')
        ax.text(0.05,0.10,tit1,color='b',transform=ax.transAxes)
        
#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()

print('read %s job yields'%len(args.jid6))

first=1
dataV=[]
thetaV=[]
for jid in args.jid6:
    inpF=args.dataPath+'%s-qubitAna.yaml'%(jid)
    blob=read_yaml(inpF)
    if first :
        print('blob keys:', blob.keys())
        first=0
        labL=[ x[2] for x in blob['data']]
        args.prjName=blob['transpName']
        tit1='num cycles=%d'%blob['noiseConf']['num_cycles']
        coD=blob['circ_info']['operations']
        dd=blob['circ_info']['depth']-coD['barrier']
        tit1+='\ncirc depth=%d; u2:%d, u3:%d, cx:%d'%(dd,coD['u2'],coD['u3'],coD['cx'])
        x0,x1,x2=blob['data'][0]
        assert '0.base.0'==x2
        oneV=[  [x0,x1] for i in labL ]
        dataV.append(oneV)
        thetaV.append( 0.)
    else:
        assert args.prjName==blob['transpName']

    oneV=[  [x[0],x[1]] for x in blob['data']]
    dataV.append(oneV)
    thetaV.append( blob['noiseConf']['theta_std'])

dataV=np.array(dataV)
thetaV=np.array(thetaV)
print('labL',labL)
print('thetaV',thetaV)
#print('dataV',dataV)
# - - - - - - - -
# PLOT RESULTS
# - - - - - - - -
    
plot=SweepPlotter(args)
plot.qubit_summary(labL,thetaV,dataV,figId=1,tit1=tit1)

plot.display_all('sweep', tight=True)

