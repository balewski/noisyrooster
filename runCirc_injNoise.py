#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
Part 2) inject single random rotation and execute

'''

# Import general libraries (needed for functions)
import time,os,sys,pytz

from datetime import datetime
from pprint import pprint
import numpy as np
from qiskit import QuantumCircuit, Aer, assemble
from qiskit.converters import circuit_to_dag

sys.path.append(os.path.abspath("./utils/"))
from Circ_Util import  read_yaml,write_yaml,circuit_summary, circ_2_yaml, yields_to_numpy, write_data_hdf5
from NoisyCircuitGenerator import NoisyDagGenerator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('-i',"--inpPath",default='circ_transp/',help="input for everything")

    parser.add_argument('-t','--transpName', default='adder_4Q+o1',
                        help=" transpiled circuit to be executed")

    parser.add_argument('-o',"--outPath",default='out',help="output path ")
    parser.add_argument('-s','--shots',type=int,default=2048, help="shots")
    parser.add_argument('-M','--noiseModel', nargs='+',default=[20,'gate',0.2], help="custom circuit noise: [ num_cycles, gate/qubit, theta_std ] ")

    parser.add_argument('-j',"--jid6",default=None ,help="overwrite natural jobId")

    args = parser.parse_args()
    args.inpPath+='/' 
    args.outPath+='/' 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [args.inpPath, args.outPath]:
        if  os.path.exists(xx): continue
        print('missing dir: ',xx)
        exit(1)

    # parse input params
    args.noiseConf={
        'num_cycles':int(args.noiseModel[0]),
        'type': args.noiseModel[1],
        'theta_std':float(args.noiseModel[2]) }
    print('noiseConf:',args.noiseConf)
    
    return args

#...!...!....................
def submitInfo_meta(job_exe, args,circD,noiseConf,expMeta):  # packs job submission data for saving
    nowT = datetime.now()
    submInfo={}
    if args.jid6==None:
        submInfo['job_id']=job_exe.job_id()
        submInfo['jid6']='j'+submInfo['job_id'][-6:] # the last 6 chars in job id , as handy job identiffier
    else:
        submInfo['job_id']='user-defined-job-id'
        submInfo['jid6']='j'+args.jid6
        
    submInfo['submitDate']=nowT.strftime("%Y-%m-%d %H:%M")
    submInfo['backName']='%s'%job_exe.backend().name()
    submInfo['transpName']=args.transpName
    submInfo['num_exp']=len(expMeta)
    submInfo['num_cycles']=args.noiseConf['num_cycles']
    submInfo['shots']=args.shots
    
    jobInfo={}
    jobInfo['noiseConf']=noiseConf
    jobInfo['baseCircMD5']=circD['md5']
    jobInfo['meas_qubit']=circD['info']['meas_qubit']
    jobInfo['exp_name']=expMeta
    jobInfo['readme']='noise sensitivity analysis 1'


    circInfo={}; coD=circD['info']
    circInfo['depth']=coD['depth']
    circInfo['operations']=coD['operations']

    outD={'subm_info':submInfo, 'job_info':jobInfo,'circ_info':circInfo}
    #pprint(outD)
    return outD


#...!...!....................
def assembly_noisyGate_array_fromDAG(baseCirc,noiseConf,verb=1):
    startT = time.time()

    #.... GENERATE NEW CIRCUITS
    circL=[]
    circGener=NoisyDagGenerator(baseCirc,verb=verb)

    circ=circGener.baseCircuit()
    #print(circ0)
    circL.append( circ) #  add base circuit for each set

    if noiseConf['theta_std']>1.e-3:  # will inject noise
        gateNameL=circGener.gateNameL
        #print('aaa gateNameL=',gateNameL)
        for gateNm in gateNameL:
            # I need decompose because I injected a custom gate
            circ=circGener.noisyGate_Circuit( gateNm, noiseConf['theta_std'],verb)
            #1circOpt=circ.decompose()
            circOpt=circ
            if verb>0: print('M:',gateNm, 'noisy transpiled to:'); print(circOpt)
            circL.append( circOpt)
            #if 'cx' in gateNm: ok342
            #break

    if verb>0:
        print('assembly_circuit_array done, circLen=%d, took %d sec'%(len(circL),time.time()-startT))

    return circL,circGener.expMeta


#...!...!....................
def assembly_noisyQubit_array_fromDAG(baseCirc,noiseConf,verb=1):
    startT = time.time()

    #.... GENERATE NEW CIRCUITS
    circL=[]
    circGener=NoisyDagGenerator(baseCirc,verb=verb)

    circ0=circGener.baseCircuit()
    #print(circ0)
    circL.append( circ0) #  add base circuit for each set

    gateNameL=circGener.gateNameL
    qubitL=circGener.qubitIndexL
    gateNameL.reverse()  # in place
    if verb>0: 
        print('aaa1 gateNameL=',gateNameL,type(gateNameL))
        print('aaa2 qubitL=',qubitL)

    #
    #qubitL=[1]  #tmp
    for qidx in qubitL: # inject nois after every gate on this qubit
        qstr=str(qidx)
        baseDag=circuit_to_dag(circ0)
        nIns=0
        for gateNm in gateNameL:
            gL=gateNm.split('.')
            #print('qidx=%d   gateNm=%s '%(qidx, gateNm),gL)
            if qstr not in gL[2:] : continue
            #print(qstr, 'match',gL[2:], gateNm )
            nIns+=1
            circ=circGener.noisyQubit_Circuit(baseDag, gateNm, noiseConf['theta_std'],qidx,verb)

        if verb>0: print('M: qidx=%d  inserted %d noise_op :'%(qidx,nIns)); print(circ)
        circ.name='%d.Q_ng.%d'%(qidx,nIns)
        circGener.expMeta.append(circ.name)

        circL.append( circ)

    if verb>0:
        print('assembly_circuit_array done, circLen=%d, took %d sec'%(len(circL),time.time()-startT))

    return circL,circGener.expMeta




#...!...!....................
def run_large_local_simu_fromDAG(baseCirc,circD,noiseConf):
    num_cycles=noiseConf['num_cycles']
    print('split into %d cycles'%num_cycles)
    backend = Aer.get_backend('qasm_simulator')
    assert backend.configuration().simulator

    #args.noiseModel[0]='1'
    #? must fake action of  yields_2_metaD() from retr_circ
    outL=[]
    outD={'subm_info':{'jid6':'zeroCycle'}, 'job_info':None}
    
    startT = time.time()
    for ic in range(num_cycles):
        if ic%30==0: print('simu cycle=',ic,'elaT=%.1f min'%((time.time()-startT)/60.))
        verb=(ic==0)
        if noiseConf['type']=='gate':
            circAr,expMetaD=assembly_noisyGate_array_fromDAG(baseCirc,noiseConf,verb=verb)
        elif noiseConf['type']=='qubit':
            circAr,expMetaD=assembly_noisyQubit_array_fromDAG(baseCirc,noiseConf,verb=verb)
        else:
            print('bad noise type=%s, ABORT'%noiseConf['type'])
            exit(11)
        # Compile and run the Quantum circuit on a simulator backend
        qobj_circ = assemble(circAr, backend, shots=args.shots)
        jobAr=backend.run(qobj_circ)
        countsD=jobAr.result().get_counts(0)
        #if verb: print('circ[0]A  counts:',countsD)
        jobHeadD = jobAr.result().to_dict()
        jobResD=jobHeadD.pop('results')
        if verb: print('circ[0]B  counts:',jobResD[0]['data']['counts'])
        numExp=len(jobResD)
        if verb:
            print('see numExp=',numExp,'job header:'); pprint(jobHeadD)
        assert jobHeadD['success']
        if outD['job_info']==None:
            outD=submitInfo_meta(jobAr,args,circD, noiseConf, expMetaD)
            nclbit=len(circD['info']['meas_qubit'])
            
        # ....   harvest results
        for exp in jobResD:
            #print('gg'); pprint(exp); ok34
            counts=exp['data']['counts']
            #counts2={ bin(int(x,16))[2:].zfill(nclbit):float(counts[x] ) for x in counts} # get rid of numpy
            expName=exp['header']['name']
            #print('rr',expMetaD,counts.keys())
            outL.append( { 'shots': exp['shots'], 'counts':counts  , 'name':expName})

    elaT=time.time() - startT
    # print(' timzeone-aware dt')
    nowT2=datetime.utcnow().replace(tzinfo=pytz.utc)
    #print(nowT2,'tmz=',nowT2.tzinfo )
    
    retrD ={'num_cycles':num_cycles,'simu_elaT':elaT,'exec_date':'%s'%nowT2}
    retrD['meta_fname']= '%s-meta.yaml'%outD['subm_info']['jid6']
    retrD['counts_fname']= '%s-counts.h5'%outD['subm_info']['jid6']
    outD['retr_info']=retrD
    print('jid6:',outD['subm_info']['jid6'])
    print('local simulation of %d cycles done, took %.1f min'%(num_cycles,elaT/60.))
    return outL, outD


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()

metaD={}
transpF=args.inpPath+'transp.'+args.transpName+'.yaml'
blobD=read_yaml(transpF)
if args.verb>2:  print('M:  transp blob'); pprint(blobD)

circD=blobD['circOpt']

if args.verb>1:  print('M: circOpt data:'); pprint(circD)

print("\n ------- Use Aer's qasm_simulator for counting")

qasmOpt=circD['qasm']
circOpt=QuantumCircuit.from_qasm_str(qasmOpt)

print('base circOpt',circOpt.name); print(circOpt)
circuit_summary(circOpt)
yieldL, outD=run_large_local_simu_fromDAG(circOpt,circD,args.noiseConf)
yieldD=yields_to_numpy(yieldL,outD) 
metaF=args.outPath+outD['retr_info']['meta_fname']
write_yaml(outD,metaF)
outH5=args.outPath+outD['retr_info']['counts_fname']
write_data_hdf5(yieldD,outH5)
print('M: run done -j ',outD['subm_info']['jid6'])

