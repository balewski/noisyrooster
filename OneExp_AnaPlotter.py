import numpy as np
import time,os,sys
from pprint import pprint
from matplotlib import cm as cmap

sys.path.append(os.path.abspath("./utils/"))
from Plotter_Backbone import Plotter_Backbone
from matplotlib.dates import DateFormatter

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class OneExp_AnaPlotter(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#...!...!....................
    def one_label_vs_date(self,one,targetLab,obsN,figId,tit1=''):        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,3))
        ax = self.plt.subplot()

        assert obsN=='counts'
        assert len(one.execT)>0
        iLab=one.labels.index(targetLab)
        print(targetLab,iLab)

        Y=one.rawY        
        
        tmV=one.execT
        yV=[]
        yV=Y[:,:,iLab] #???
        tm0=min(tmV)
        #print('minT',tm0)
        dtmV=[ (x-tm0).total_seconds()/3600. for x in tmV ]
        #print('ddd',dtmV)
        #print('jid',one.jid6L)
                
        ax.plot(dtmV , yV,  'o')
        # do box-plot instead
        #1yB=[ yV[i,:] for i in range(yV.shape[0]) ]
        #1dtmV=[ float('%.1f'%x) for x in dtmV ]
        #1ax.boxplot(yB,labels=dtmV, showfliers=False) # last is no-outliers

        tit='meas=%s, num jobs=%d, shots=%d, %s'%(targetLab,Y.shape[0],one.shots,tit1)
        ax.set(title=tit, xlabel='wall hours since: %s'%tm0, ylabel='%s'%obsN)
        ax.grid()

        for a,b,t in zip(dtmV,yV[:,0],one.jid6L): #
            ax.text(a,b,t,rotation=60)

            
#...!...!....................
    def histo_per_label(self,one,obsN,figId):
        nrow,ncol=1,8
        if len(one.labels) >8:  nrow=2
        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(15,2*nrow))        
        if obsN=='prob':
            pmax=1.02
            binsX=np.linspace(0., pmax,30)
            #binsX=np.linspace(0.55, 0.85,40)
        else:
            wrong_prob_name
              
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        for idx,lab in enumerate(one.labels):
            ax = self.plt.subplot(nrow,ncol,1+idx)
            ax.set(title='meas=%s'%(lab), xlabel=obsN, ylabel='experiments')
            yA=one.dataV[obsN][idx]
            ax.hist(yA.flatten(),bins=binsX)
            ax.grid()

            avr,std,num,err=one.dataV['avr_'+obsN][idx]
            txt2='avr: %.3g\n  +/-%.2g'%(avr,err)
            if idx==0: txt2+='\nexp %s'%one.expId
            if idx==1: txt2+='\nsum %d'%num
            ax.text(0.15,0.5,txt2,color='b',transform=ax.transAxes)

#...!...!....................
    def experiment_summary(self,one,obsN,figId):

        yA=one.dataV[obsN] # [label][experiments]
        print('P: ex0', yA.shape)
        [numLab,numCyc]=yA.shape

        yIn=3
        if numLab>8 : yIn=numLab/5
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,yIn))
        ax = self.plt.subplot()

        yB=[ yA[i,:].flatten() for i in range(yA.shape[0]) ]
        
        ax.boxplot(yB,labels=one.labels,vert=False, showfliers=False) # last is no-outliers

        tit=one.metaD['subm_info']['transpName']+', exp:%s, %s'%(one.expId,str(one.jid6L[0]))
        if len(one.jid6L[0])>0: tit+=" x %d"%len(one.jid6L)

        ax.set(title=tit, xlabel=obsN+', shots=%d'%(one.shots) ,ylabel='measured bit-string')
        ax.grid()

        #ax.set_xlim(0,pmax)

        #print('eee',one.metaD.keys())
        txt1='exec_date '+one.metaD['retr_info']['exec_date'].replace(' ','\n')
        
        txt1=txt1.replace('+00:00','_UTC')

        coD=one.metaD['circ_info']['operations']
        dd=one.metaD['circ_info']['depth']-coD['barrier']
        txt1+='\ncirc depth=%d; u2:%d, u3:%d, cx:%d'%(dd,coD['u2'],coD['u3'],coD['cx'])

        ax.text(0.1,0.4,txt1,transform=ax.transAxes,color='b')


        
#...!...!....................
    def multi_experiment_gate(self,oneD,lab,expIdL,figId):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(14,5))

        obsN='avr_prob'
        print('muti_experiment, label=',lab)
        oneBase=oneD['0.base.0']
        totRunTime=0
        for x in oneBase.runTimeL: totRunTime+=x
        sigTheta=oneBase.metaD['job_info']['noiseConf']['theta_std']
        txt1=oneBase.metaD['subm_info']['transpName']+', nRun=%d,  elaT=%.2f h, jid: %s\nsigTheta=%.2f'%(oneBase.rawY.shape[1],totRunTime/3600.,str(oneBase.jid6L),sigTheta)
        #print('txt1=',txt1)
        tl=140
        if len(txt1) >tl : txt1=txt1[:tl]+' <clip>'

        # the single bit-string index we want to plot
        iLab=oneBase.labels.index(lab)
        print('P2:mtl',lab,iLab)
                
        dataV=[]  # all data to be plotted
        for expId in expIdL:
            one=oneD[expId]
            (avr,std,num,err)=one.dataV[obsN][iLab] # [label][experiments]
            dataV.append([float(avr),float(err),expId])
                     
        # divide dataV by type of gate and 1st qubit
        cL10 = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'] # 10 distinct colors
        gateL=['base','u2','u3','cx','bad']
        mL=['>','$2$','$3$','$x$']
        mK=4
        dD=[{'x':[],'y':[],'ey':[], 'ec':[],'mc':'black','m':mL[i],'lab':gateL[i]}  for  i in  range(mK)]

        #print('dD',dD)
        xI=[]
        for i, [avr,err,expId] in enumerate(dataV):
            #print ('zzz',i, [avr,err,expId])
            xI.append(i)
            for j, xx in enumerate(gateL):
                if xx not in expId: continue
                break
            q1=int(expId.split('.')[2])
            #print('gg', j,xx,expId,q1)
            D=dD[j]
            D['x'].append(i)
            D['y'].append(avr)
            D['ey'].append(err)
            D['ec'].append(cL10[q1])

        ax = self.plt.subplot()
        for i  in  range(mK):  
            ax.errorbar(dD[i]['x'], dD[i]['y'], yerr= dD[i]['ey'], marker= dD[i]['m'], fmt='o', ecolor=dD[i]['ec'],color=dD[i]['mc'], label=dD[i]['lab'])

        
        ax.legend(loc='upper right', title='ecolor=q1')

        yy=dataV[0][0]
        ax.axhline(yy, linestyle='--', linewidth=1)

        y00=0.2
        if obsN=='avr_prob': y00=0.8
        ax.text(0.1,y00,txt1,color='b',transform=ax.transAxes)

        ax.set(title=oneBase.metaD['subm_info']['transpName']+' (meas=%s), noise injected at 1 gate of %d'%(lab,len(xI)),  xlabel='noise injection gate index:  circ_layer.gate_name.qubit(s)', ylabel=obsN)
        ax.grid()
        ax.set_xticks(xI)
        ax.set_xticklabels(expIdL,rotation=80)
        return dataV


#...!...!....................
    def multi_experiment_qubit(self,oneD,lab,expIdL,figId):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))

        obsN='avr_prob'
        print('muti_experiment_QN, label=',lab)
        oneBase=oneD['0.base.0']
        totRunTime=0
        for x in oneBase.runTimeL: totRunTime+=x

        sigTheta=oneBase.metaD['job_info']['noiseConf']['theta_std']
        txt1=oneBase.metaD['subm_info']['transpName']+', nRun=%d,  elaT=%.2f h, jid: %s\nsigTheta=%.2f'%(oneBase.rawY.shape[1],totRunTime/3600.,str(oneBase.jid6L),sigTheta)
        #print('txt1=',txt1)
        tl=140
        if len(txt1) >tl : txt1=txt1[:tl]+' <clip>'

        # the single bit-string index we want to plot
        iLab=oneBase.labels.index(lab)
        print('P2:mtl',lab,iLab)

        dataV=[]  # all data to be plotted
        dataV1=[] #data tpo be saved as yaml
        for expId in expIdL:
            one=oneD[expId]
            (avr,std,num,err)=one.dataV[obsN][iLab] # [label][experiments]
            dataV.append([avr,err])
            dataV1.append([float(avr),float(err),expId])
            
        dataV=np.array(dataV)
        xI=[i for i in range(dataV.shape[0]) ]
        #print('ddd',dataV,xI)

        ax = self.plt.subplot()
        ax.errorbar(xI, dataV[:,0], yerr= dataV[:,1], fmt='o')

        yy=dataV[0][0]
        ax.axhline(yy, linestyle='--', linewidth=1)

        y00=0.2
        if obsN=='avr_prob': y00=0.8
        ax.text(0.1,y00,txt1,color='b',transform=ax.transAxes)

        ax.set(title=oneBase.metaD['subm_info']['transpName']+' (meas=%s), noise injected at 1 qubit of %d'%(lab,len(xI)-1),  xlabel='noisy qubit, q.ngate', ylabel=obsN)
        ax.grid()
        ax.set_xticks(xI)
        ax.set_xticklabels(expIdL,rotation=80)
        return dataV1
